package com.vironit.trainingproject;

import android.app.IntentService;
import android.content.Intent;
import android.widget.Toast;

public class MyIntentService extends IntentService {
    private long startTime, endTime;

    public MyIntentService() {
        super("MyIS");
    }

    @Override
    public void onCreate() {
        Toast.makeText(this, "Sorting is start", Toast.LENGTH_SHORT).show();
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        endTime = System.currentTimeMillis();
        long diffTime = endTime - startTime;
        Toast.makeText(this, "ServiceIntent sorting is done. Time: " + diffTime, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        startTime = System.currentTimeMillis();
        Singleton.sorting();

    }
}
