package com.vironit.trainingproject;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.vironit.trainingproject.DataBase.PersonRepository;

import java.util.List;

public class PersonViewModel extends AndroidViewModel {
    private PersonRepository repository;
    private LiveData<List<Person>> allPersons;

    public PersonViewModel(@NonNull Application application) {
        super(application);
        repository = new PersonRepository(application);
        allPersons = repository.getAll();
    }

    public void insert(Person person) {
        repository.insert(person);
    }

    public void update(Person person) {
        repository.update(person);
    }

    public void delete(Person person) {
        repository.delete(person);
    }

    public LiveData<List<Person>> getAll() {
        return allPersons;
    }

    public LiveData<Person> getById(long id) {
        return repository.getById(id);
    }
}
