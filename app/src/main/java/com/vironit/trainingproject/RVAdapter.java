package com.vironit.trainingproject;

import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

public class RVAdapter extends RecyclerView.Adapter<RVAdapter.ViewHolder> {
    private List<Person> persons = new ArrayList<>();
    private OnItemClickListener onItemClickListener;
    private OnItemCallClickListener onItemCallClickListener;
    private boolean circle1;

    public RVAdapter(boolean circle) {
        this.circle1 = circle;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.person_item, viewGroup, false);
        return new ViewHolder(itemView, circle1);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        Person currentPerson = persons.get(position);

        RequestOptions requestOptions;
        boolean circle = viewHolder.circle;
        if (circle) {
            requestOptions = RequestOptions.circleCropTransform();
        } else {
            requestOptions = RequestOptions.centerCropTransform();
        }
        Glide.with(viewHolder.imageViewPhoto.getContext())
                .asBitmap()
                .load(currentPerson.getPhotoId())
                .apply(requestOptions)
                .into(viewHolder.imageViewPhoto);

        viewHolder.textViewName.setText(currentPerson.getName());
        viewHolder.textViewSurname.setText(currentPerson.getSurname());
        viewHolder.textViewPhoneNumber.setText(currentPerson.getPhoneNumber());
    }

    @Override
    public int getItemCount() {
        return persons.size();
    }

    public void setPersons(List<Person> persons) {
        this.persons = persons;
        notifyDataSetChanged();
    }

    public Person getPersonAt(int position) {
        return persons.get(position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView imageViewPhoto;
        private TextView textViewName;
        private TextView textViewSurname;
        private TextView textViewPhoneNumber;
        private boolean circle;

        public ViewHolder(@NonNull View itemView, boolean circle) {
            super(itemView);
            imageViewPhoto = itemView.findViewById(R.id.iv_photo);
            textViewName = itemView.findViewById(R.id.tv_name);
            textViewSurname = itemView.findViewById(R.id.tv_surname);
            textViewPhoneNumber = itemView.findViewById(R.id.tv_phone_number);
            this.circle = circle;

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (onItemClickListener != null && position != RecyclerView.NO_POSITION) {
                        onItemClickListener.onItemClick(persons.get(position));
                    }
                }
            });

            FloatingActionButton fabCall = itemView.findViewById(R.id.fab_call);
            fabCall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (onItemCallClickListener != null && position != RecyclerView.NO_POSITION) {
                        onItemCallClickListener.onItemCallClick(persons.get(position));
                    }
                }
            });

        }
    }

    public interface OnItemClickListener {
        void onItemClick(Person person);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemCallClickListener {
        void onItemCallClick(Person person);
    }

    public void setOnItemCallClickListener(OnItemCallClickListener onItemCallClickListener) {
        this.onItemCallClickListener = onItemCallClickListener;
    }
}
