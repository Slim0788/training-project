package com.vironit.trainingproject;

import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

public class PersonInformActivity extends AppCompatActivity {
    public static final String EXTRA_ID =
            "com.vironit.trainingproject.EXTRA_ID";
    public static final String EXTRA_NAME =
            "com.vironit.trainingproject.EXTRA_NAME";
    public static final String EXTRA_SURNAME =
            "com.vironit.trainingproject.EXTRA_SURNAME";
    public static final String EXTRA_PHONE_NUMBER =
            "com.vironit.trainingproject.EXTRA_PHONE_NUMBER";
    public static final String EXTRA_PHOTO =
            "com.vironit.trainingproject.EXTRA_PHOTO";

    static final String SAVED_VARIABLE = "avatar_form";
    static final String PREF_NAME = "MySettings";

    private ImageView imageViewPersonPhoto;
    private TextView TextViewName;
    private TextView TextViewSurname;
    private TextView TextViewPhoneNumber;

    private PersonViewModel personViewModel;
    private Person personForDell;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person_inform);

        imageViewPersonPhoto = findViewById(R.id.photo);
        imageViewPersonPhoto.startAnimation(AnimationUtils.loadAnimation(this, R.anim.animation_scale));

        TextViewName = findViewById(R.id.tv_name);
        TextViewName.startAnimation(AnimationUtils.loadAnimation(this, R.anim.animation_translate));

        TextViewSurname = findViewById(R.id.tv_surName);
        TextViewSurname.startAnimation(AnimationUtils.loadAnimation(this, R.anim.animation_translate));

        TextViewPhoneNumber = findViewById(R.id.tv_phoneNumber);
        TextViewPhoneNumber.startAnimation(AnimationUtils.loadAnimation(this, R.anim.animation_translate));

        Intent intent = getIntent();
        long id = intent.getLongExtra(EXTRA_ID, 1);

        TextViewName.setText(intent.getStringExtra(EXTRA_NAME));
        TextViewSurname.setText(intent.getStringExtra(EXTRA_SURNAME));
        TextViewPhoneNumber.setText(intent.getStringExtra(EXTRA_PHONE_NUMBER));
        TextViewName.setText(intent.getStringExtra(EXTRA_NAME));
        imageViewPersonPhoto.setImageResource(intent.getIntExtra(EXTRA_PHOTO, 1));

        SharedPreferences settings = getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        boolean circle = settings.getBoolean(SAVED_VARIABLE, true);

        RequestOptions requestOptions;
        if (circle) {
            requestOptions = RequestOptions.circleCropTransform();
        } else {
            requestOptions = RequestOptions.centerCropTransform();
        }
        Glide.with(imageViewPersonPhoto.getContext())
                .asBitmap()
                .load(intent.getIntExtra(EXTRA_PHOTO, 1))
                .apply(requestOptions)
                .into(imageViewPersonPhoto);

        personViewModel = new PersonViewModel(getApplication());
        personViewModel.getById(id).observe(this, new Observer<Person>() {
            @Override
            public void onChanged(@Nullable Person person) {
                personForDell = person;

                //если так задавать, то при deletePerson крашится
//                TextViewName.setText(person.getName());
//                TextViewSurname.setText(person.getSurname());
//                TextViewPhoneNumber.setText(person.getPhoneNumber());

            }
        });
    }

    public void deletePerson(View v) {
        personViewModel.delete(personForDell);
        Toast.makeText(PersonInformActivity.this, "Person deleted", Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_options, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save_file:
                Toast.makeText(this, "save file method", Toast.LENGTH_SHORT).show();
                DialogFragment dialog = new DialogFragment();
                dialog.show(getSupportFragmentManager(), "custom");
                break;
            case R.id.action_settings:
                Toast.makeText(this, "go to settings", Toast.LENGTH_SHORT).show();
                Intent intentSettings = new Intent(this, SettingsActivity.class);
                startActivity(intentSettings);
                break;
            case R.id.bubble_sorting:
                Toast.makeText(this, "go to sorting method", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(this, SortingActivity.class));
                break;
            case R.id.action_exit:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
