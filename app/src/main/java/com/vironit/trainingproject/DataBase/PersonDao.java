package com.vironit.trainingproject.DataBase;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.vironit.trainingproject.Person;

import java.util.List;

@Dao
public interface PersonDao {

    @Insert
    void insert(Person person);

    @Update
    void update(Person person);

    @Delete
    void delete(Person person);

    @Query("SELECT * FROM person_table")
    LiveData<List<Person>> getAll();

    @Query("SELECT * FROM person_table WHERE id = :id")
    LiveData<Person> getById(long id);
}
