package com.vironit.trainingproject.DataBase;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.vironit.trainingproject.Person;

@Database(entities = {Person.class}, version = 1, exportSchema = false)
public abstract class PersonDatabase extends RoomDatabase {

    public abstract PersonDao personDao();

    private static PersonDatabase instance;

    public static synchronized PersonDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    PersonDatabase.class, "person_table")
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return instance;
    }

    //    начальное наполнение базы почему-то не работает

    private static RoomDatabase.Callback roomCallback = new RoomDatabase.Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new PopulateDbAsyncTask(instance).execute();
        }
    };

    private static class PopulateDbAsyncTask extends AsyncTask<Void, Void, Void> {
        private PersonDao personDao;

        private PopulateDbAsyncTask(PersonDatabase db) {
            personDao = db.personDao();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            personDao.insert(new Person("Tom", "Rider", "684343421", 10));
            personDao.insert(new Person("Bob", "Hanks", "13546513534", 10));
            personDao.insert(new Person("Rayan", "Raynolds", "368461646", 10));
            personDao.insert(new Person("Jim", "Parsons", "911", 10));
            return null;
        }
    }
}
