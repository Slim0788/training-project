package com.vironit.trainingproject;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private final String LOG = getClass().getSimpleName();

    private final static String FILE_NAME = "token.txt";

    public static final int ADD_PERSON_REQUEST = 1;
    public static final int INFORM_PERSON_REQUEST = 2;

    static final String SAVED_VARIABLE = "avatar_form";
    static final String PREF_NAME = "MySettings";

    private BroadcastReceiver broadcastReceiver;
    private PersonViewModel personViewModel;
    private RecyclerView recyclerView;
    private RVAdapter rvAdapter;
    boolean circle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FloatingActionButton fabAddPerson = findViewById(R.id.button_add_person);
        fabAddPerson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AddPersonActivity.class);
                startActivityForResult(intent, ADD_PERSON_REQUEST);
            }
        });

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true); //для ускорения работы

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                int WiFiState = intent.getIntExtra(
                        WifiManager.EXTRA_WIFI_STATE, 0);
//                        Integer.parseInt(WifiManager.EXTRA_PREVIOUS_WIFI_STATE));

                switch (WiFiState) {
                    case WifiManager.WIFI_STATE_DISABLED:
                        Snackbar.make(recyclerView, "WiFi is disabled", Snackbar.LENGTH_LONG).show();
                        break;
                    case WifiManager.WIFI_STATE_ENABLED:
                        Snackbar.make(recyclerView, "WiFi is enabled", Snackbar.LENGTH_LONG).show();
                        break;
                    case WifiManager.WIFI_STATE_DISABLING:
                        Snackbar.make(recyclerView, "WiFi is disabling", Snackbar.LENGTH_LONG).show();
                        break;
                    case WifiManager.WIFI_STATE_ENABLING:
                        Snackbar.make(recyclerView, "WiFi is enabling", Snackbar.LENGTH_LONG).show();
                        break;
                    case WifiManager.WIFI_STATE_UNKNOWN:
                        Snackbar.make(recyclerView, "WiFi is unknown", Snackbar.LENGTH_LONG).show();
                        break;
                }
            }
        };

        registerReceiver(broadcastReceiver, new IntentFilter(WifiManager.WIFI_STATE_CHANGED_ACTION));


        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                personViewModel.delete(rvAdapter.getPersonAt(viewHolder.getAdapterPosition()));
                Toast.makeText(MainActivity.this, "Person deleted", Toast.LENGTH_SHORT).show();
            }
        }).attachToRecyclerView(recyclerView);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences settings = this.getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        circle = settings.getBoolean(SAVED_VARIABLE, true);

        rvAdapter = new RVAdapter(circle);
        recyclerView.setAdapter(rvAdapter);

        personViewModel = ViewModelProviders.of(this).get(PersonViewModel.class);
        personViewModel.getAll().observe(this, new Observer<List<Person>>() {
            @Override
            public void onChanged(@Nullable List<Person> people) {
                rvAdapter.setPersons(people);
            }
        });

        rvAdapter.setOnItemClickListener(new RVAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Person person) {
                Intent intent = new Intent(MainActivity.this, PersonInformActivity.class);
                intent.putExtra(PersonInformActivity.EXTRA_ID, person.getId());
                intent.putExtra(PersonInformActivity.EXTRA_NAME, person.getName());
                intent.putExtra(PersonInformActivity.EXTRA_SURNAME, person.getSurname());
                intent.putExtra(PersonInformActivity.EXTRA_PHONE_NUMBER, person.getPhoneNumber());
                intent.putExtra(PersonInformActivity.EXTRA_PHOTO, person.getPhotoId());
                startActivityForResult(intent, INFORM_PERSON_REQUEST);
            }
        });

        rvAdapter.setOnItemCallClickListener(new RVAdapter.OnItemCallClickListener() {
            @Override
            public void onItemCallClick(Person person) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + person.getPhoneNumber()));

                startActivity(intent);

            }
        });

    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ADD_PERSON_REQUEST && resultCode == RESULT_OK) {
            String name = data.getStringExtra(AddPersonActivity.EXTRA_NAME);
            String surname = data.getStringExtra(AddPersonActivity.EXTRA_SURNAME);
            String phoneNumber = data.getStringExtra(AddPersonActivity.EXTRA_PHONE_NUMBER);
            int photo = data.getIntExtra(AddPersonActivity.EXTRA_PHOTO, 1);

            Person person = new Person(name, surname, phoneNumber, photo);
            personViewModel.insert(person);

            Toast.makeText(this, "Person saved", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Person not saved", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_options, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_save_file:
                Toast.makeText(this, "save file method", Toast.LENGTH_SHORT).show();
                DialogFragment dialog = new DialogFragment();
                dialog.show(getSupportFragmentManager(), "custom");
                break;
            case R.id.action_settings:
                Toast.makeText(this, "go to settings", Toast.LENGTH_SHORT).show();
                Intent intentSettings = new Intent(this, SettingsActivity.class);
                startActivity(intentSettings);
                break;
            case R.id.bubble_sorting:
                Toast.makeText(this, "go to sorting method", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(this, SortingActivity.class));
                break;
            case R.id.action_exit:
//                Crashlytics.getInstance().crash();     //Test crash for Firebase
//                getInstance();      //get instance

                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    protected void getInstance() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Toast.makeText(MainActivity.this, "getInstanceId failed", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        // Get new Instance ID token
                        String token = task.getResult().getToken();
                        // Log and toast
                        Log.i("Instance", token);
                        Toast.makeText(MainActivity.this, token, Toast.LENGTH_LONG).show();

                        //save to file
                        FileOutputStream fos = null;
                        try {
                            fos = new FileOutputStream(getExternalPath());
                            Log.i(LOG, "fos открыт");

                            fos.write(token.getBytes());

                            Log.i(LOG, "Файл сохранен");
                            Toast.makeText(MainActivity.this, "Файл сохранен", Toast.LENGTH_SHORT).show();

                        } catch (IOException ex) {
                            Log.i(LOG, "исключение: " + ex.getMessage());
                            Toast.makeText(MainActivity.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
                        } finally {
                            try {
                                if (fos != null)
                                    fos.close();
                                Log.i(LOG, "fos закрыт");
                            } catch (IOException ex) {
                                Toast.makeText(MainActivity.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
                                Log.i(LOG, "исключение: " + ex.getMessage());
                            }
                        }
                    }
                });
    }

    private File getExternalPath() {
        return (new File(Environment.getExternalStorageDirectory(), FILE_NAME));
    }

}
