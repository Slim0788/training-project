package com.vironit.trainingproject;

import android.Manifest;
import android.app.Dialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class DialogFragment extends AppCompatDialogFragment {

    private final String LOG = getClass().getSimpleName();

    private final static String FILE_NAME = "persons.txt";
    private static final int REQUEST_PERMISSION_WRITE = 1001;
    private boolean permissionGranted;
    static String text;
    private PersonViewModel personViewModel;

    private static List<Person> persons = new ArrayList<>();


    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        personViewModel = ViewModelProviders.of(this).get(PersonViewModel.class);
        personViewModel.getAll().observe(this, new Observer<List<Person>>() {
            @Override
            public void onChanged(@Nullable List<Person> people) {
                persons = people;
            }
        });

        saveFile();

        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(getActivity()));
        builder
                .setTitle("Send to Email")
                .setView(R.layout.dialog)
                .setPositiveButton("Send", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

//                        Log.i(LOG, "number of EditText et_mail: " + String.valueOf(R.id.et_email));
//                        EditText textTo = getView().findViewById(R.id.et_email);
//                        String mail = textTo.getText().toString();

                        Intent email = new Intent(Intent.ACTION_SEND);
                        Uri uri = Uri.fromFile(getExternalPath());
                        Log.i(LOG, "путь к фалу: " + String.valueOf(uri));
                        email.putExtra(Intent.EXTRA_EMAIL, new String[]{"qwerty@mail.com"});
//                        email.putExtra(Intent.EXTRA_EMAIL, new String[]{mail});
                        email.putExtra(Intent.EXTRA_TEXT, text);
                        email.putExtra(Intent.EXTRA_STREAM, uri);
//                        email.setType("text/plain");
                        email.setType("message/rfc822");
                        startActivity(Intent.createChooser(email, "Выберите email клиент :"));

                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        return builder.create();
    }

    private File getExternalPath() {
        return (new File(Environment.getExternalStorageDirectory(), FILE_NAME));
    }

    // проверяем, доступно ли внешнее хранилище для чтения и записи
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

    // проверяем, доступно ли внешнее хранилище хотя бы только для чтения
    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        return (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state));
    }

    // сохранение файла
    public void saveFile() {

        Log.i(LOG, "Вызван метод saveFile");

        checkPermissions();

        FileOutputStream fos = null;
        try {

            fos = new FileOutputStream(getExternalPath());
            Log.i(LOG, "fos открыт");
            for (Person person : persons) {
                fos.write(person.toString().getBytes());
                Log.i(LOG, person.toString());
            }
            Log.i(LOG, "Файл сохранен");
            Toast.makeText(getContext(), "Файл сохранен", Toast.LENGTH_SHORT).show();

        } catch (IOException ex) {
            Log.i(LOG, "исключение: " + ex.getMessage());
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        } finally {
            try {
                if (fos != null)
                    fos.close();
                Log.i(LOG, "fos закрыт");
            } catch (IOException ex) {

                Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
                Log.i(LOG, "исключение: " + ex.getMessage());
            }
        }
    }

    private boolean checkPermissions() {

        if (!isExternalStorageReadable() || !isExternalStorageWritable()) {
            Toast.makeText(getContext(), "Внешнее хранилище не доступно", Toast.LENGTH_LONG).show();
            Log.i(LOG, "Внешнее хранилище не доступно");
            return false;
        }
        int permissionCheck = ContextCompat.checkSelfPermission(Objects.requireNonNull(getContext()), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_PERMISSION_WRITE);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSION_WRITE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    permissionGranted = true;
                    Toast.makeText(getContext(), "Разрешения получены", Toast.LENGTH_LONG).show();
                    Log.i(LOG, "Разрешения получены");
                } else {
                    Toast.makeText(getContext(), "Необходимо дать разрешения", Toast.LENGTH_LONG).show();
                    Log.i(LOG, "Необходимо дать разрешения");
                }
                break;
        }
    }

}
