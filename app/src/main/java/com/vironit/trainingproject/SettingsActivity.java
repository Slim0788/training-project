package com.vironit.trainingproject;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class SettingsActivity extends AppCompatActivity {
    private TextView selection;
    private boolean circle;
    private SharedPreferences.Editor prefEditor;

    static final String SAVED_VARIABLE = "avatar_form";
    static final String PREF_NAME = "MySettings";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        RadioGroup radGrp = findViewById(R.id.rg_radios);
        selection = findViewById(R.id.selection);
        selection.setText("Выбрать форму аватарки");

        final SharedPreferences settings = getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        circle = settings.getBoolean(SAVED_VARIABLE, true);

        if (circle) {
            radGrp.check(R.id.rb_circle);
        } else {
            radGrp.check(R.id.rb_rectangle);
        }
        radGrp.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup rg, int id) {
                prefEditor = settings.edit();
                switch (id) {
                    case R.id.rb_circle:
                        circle = true;
                        prefEditor.putBoolean(SAVED_VARIABLE, true);
                        selection.setText("Аватарки будут круглые");
                        break;
                    case R.id.rb_rectangle:
                        circle = false;
                        prefEditor.putBoolean(SAVED_VARIABLE, false);
                        selection.setText("Аватарки будут квадратные");
                        break;
                    default:
                        break;
                }
                prefEditor.apply();
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        Toast.makeText(this, "Settings have been saved", Toast.LENGTH_SHORT).show();
    }

}
