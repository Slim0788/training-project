package com.vironit.trainingproject;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.Objects;

public class AddPersonActivity extends AppCompatActivity {
    public static final String EXTRA_NAME =
            "com.vironit.trainingproject.EXTRA_NAME";
    public static final String EXTRA_SURNAME =
            "com.vironit.trainingproject.EXTRA_SURNAME";
    public static final String EXTRA_PHONE_NUMBER =
            "com.vironit.trainingproject.EXTRA_PHONE_NUMBER";
    public static final String EXTRA_PHOTO =
            "com.vironit.trainingproject.EXTRA_PHOTO";

    private EditText editTextName;
    private EditText editTextSurname;
    private EditText editTextPhoneNumber;
    private ImageView imageViewPhoto;

    private Bundle bundle = null;

    static final String SAVED_VARIABLE = "avatar_form";
    static final String PREF_NAME = "MySettings";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_person);
        editTextName = findViewById(R.id.et_name);
        editTextSurname = findViewById(R.id.et_surname);
        editTextPhoneNumber = findViewById(R.id.et_phone_number);
        imageViewPhoto = findViewById(R.id.iv_addPhoto);

        SharedPreferences settings = getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        boolean circle = settings.getBoolean(SAVED_VARIABLE, true);

        RequestOptions requestOptions;
        if (circle) {
            requestOptions = RequestOptions.circleCropTransform();
        } else {
            requestOptions = RequestOptions.centerCropTransform();
        }
        Glide.with(this)
                .asBitmap()
                .load(R.drawable.avatar)
                .apply(requestOptions)
                .into(imageViewPhoto);

        Objects.requireNonNull(getSupportActionBar()).setHomeAsUpIndicator(R.drawable.ic_close);
    }

//    @Override
//    protected void onStart() {
//        super.onStart();
//        bundle = getIntent().getExtras();
//        if (bundle != null) {
//            editTextName.setText(getIntent().getStringExtra("name"));
//        } else {
//            savePerson();
////            Toast.makeText(this, "тест", Toast.LENGTH_SHORT).show();
//        }
//    }

    private void savePerson() {
        String name = editTextName.getText().toString();
        String surname = editTextSurname.getText().toString();
        String phoneNumber = editTextPhoneNumber.getText().toString();
        int photoId = this.getResources().getIdentifier("avatar", "drawable", this.getPackageName());

        if (name.trim().isEmpty() || surname.trim().isEmpty() || phoneNumber.trim().isEmpty()) {
            Toast.makeText(this, "Please insert a name, surname and phone number", Toast.LENGTH_SHORT).show();
            return;
        }
        Intent data = new Intent();
        data.putExtra(EXTRA_NAME, name);
        data.putExtra(EXTRA_SURNAME, surname);
        data.putExtra(EXTRA_PHONE_NUMBER, phoneNumber);
        data.putExtra(EXTRA_PHOTO, photoId);

        setResult(RESULT_OK, data);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_person_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save_note:
                savePerson();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
