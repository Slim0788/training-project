package com.vironit.trainingproject;

import android.app.IntentService;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import static com.vironit.trainingproject.R.layout.activity_sorting;

public class SortingActivity extends AppCompatActivity {

    private final int SORTING_START = 0;
    private final int SORTING_DONE = 1;

    private ImageView iv_picture;
    private Button btn_uiThread;
    private Button btn_backThread;
    private Button btn_handler;
    private Button btn_service_intent;
    private Button btn_asyncTask;

    private Handler handler;
    private long startTime, endTime;
    private View view; //костыль

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(activity_sorting);

        btn_uiThread = findViewById(R.id.btn_ui_thread);
        btn_backThread = findViewById(R.id.btn_back_thread);
        btn_handler = findViewById(R.id.btn_handler);
        btn_service_intent = findViewById(R.id.btn_service_intent);
        btn_asyncTask = findViewById(R.id.btn_asynctask);

        iv_picture = findViewById(R.id.iv_waiting);
        iv_picture.startAnimation(AnimationUtils.loadAnimation(this, R.anim.animation_scale_thread));
        Glide.with(this)
                .load(R.drawable.avatar)
                .apply(RequestOptions.circleCropTransform())
                .into(iv_picture);

        handler = new Handler() {

            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.what) {
                    case SORTING_START:
                        Snackbar.make(view, "Sorting is start", Snackbar.LENGTH_SHORT).show();
                        break;
                    case SORTING_DONE:
                        long diffTime = endTime - startTime;
                        Snackbar.make(view, "Sorting on Handler thread is done. Time: " + diffTime, Snackbar.LENGTH_SHORT).show();
                        break;
                }
            }
        };

        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                view = v;   //костыль
                switch (view.getId()) {
                    case R.id.btn_ui_thread:
                        uiThread();
                        break;
                    case R.id.btn_back_thread:
                        backThread();
                        break;
                    case R.id.btn_handler:
                        handlerThread();
                        break;
                    case R.id.btn_service_intent:
                        serviceIntentThread();
                        break;
                    case R.id.btn_asynctask:
                        asyncTaskThread();
                        break;
                }
            }
        };

        btn_uiThread.setOnClickListener(onClickListener);
        btn_backThread.setOnClickListener(onClickListener);
        btn_handler.setOnClickListener(onClickListener);
        btn_service_intent.setOnClickListener(onClickListener);
        btn_asyncTask.setOnClickListener(onClickListener);
    }

    public void uiThread() {
        Snackbar.make(view, "Sorting is start", Snackbar.LENGTH_SHORT).show();
        startTime = System.currentTimeMillis();
        Singleton.sorting();
        endTime = System.currentTimeMillis();
        long diffTime = endTime - startTime;
        Snackbar.make(view, "Sorting on UI thread is done. Time: " + diffTime, Snackbar.LENGTH_SHORT).show();
    }

    public void backThread() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Snackbar.make(view, "Sorting is start", Snackbar.LENGTH_SHORT).show();
                startTime = System.currentTimeMillis();
                Singleton.sorting();
                endTime = System.currentTimeMillis();
                long diffTime = endTime - startTime;
                Snackbar.make(view, "Sorting on background thread is done. Time: " + diffTime, Snackbar.LENGTH_SHORT).show();
            }
        }).start();
    }

    public void asyncTaskThread() {
        new AsyncTaskThread().execute();
    }

    public void serviceIntentThread() {
        startService(new Intent(this, MyIntentService.class)
                .putExtra("some task name","some task"));
    }

    public void handlerThread() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                handler.sendEmptyMessage(SORTING_START);
                startTime = System.currentTimeMillis();
                Singleton.sorting();
                endTime = System.currentTimeMillis();
                handler.sendEmptyMessage(SORTING_DONE);
            }
        }).start();
    }

    class AsyncTaskThread extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            Snackbar.make(view, "Sorting is start", Snackbar.LENGTH_SHORT).show();
            startTime = System.currentTimeMillis();
            Singleton.sorting();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            endTime = System.currentTimeMillis();
            long diffTime = endTime - startTime;
            Snackbar.make(view, "AsyncTask sorting is done. Time: " + diffTime, Snackbar.LENGTH_SHORT).show();
            super.onPostExecute(aVoid);
        }
    }

}
