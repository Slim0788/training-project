package com.vironit.trainingproject;

import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

public class FCMService extends FirebaseMessagingService {

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        if (remoteMessage.getData() != null) {
            Map<String, String> data = remoteMessage.getData();
            String name = data.get("Name");
            String surname = data.get("Surname");
            String phoneNumber = data.get("PhoneNumber");
            String photoId = data.get("Photo");

            Intent intent = new Intent(this, AddPersonActivity.class);

            intent.putExtra(AddPersonActivity.EXTRA_NAME, name);
            intent.putExtra(AddPersonActivity.EXTRA_SURNAME, surname);
            intent.putExtra(AddPersonActivity.EXTRA_PHONE_NUMBER, phoneNumber);
//            intent.putExtra(AddPersonActivity.EXTRA_PHOTO, Integer.parseInt(photoId));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

//            setResult(RESULT_OK, intent);

            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.ic_person_add)
                    .setWhen(System.currentTimeMillis())
                    .setContentTitle("New Person")
                    .setContentText(name)
                    .setContentInfo("Click to add")
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.avatar))
                    .setContentIntent(pendingIntent)
                    .setAutoCancel(true);

            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);

            notificationManager.notify(1, notificationBuilder.build());
        }
    }

}
